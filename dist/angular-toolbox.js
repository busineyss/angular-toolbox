(function (angular) {

    // Create all modules and define dependencies to make sure they exist
    // and are loaded in the correct order to satisfy dependency injection
    // before all nested files are concatenated by Gulp

    // Config
    angular.module('angular-toolbox.config', [])
            .value('angular-toolbox.config', {
                debug: true
            });

    // Modules

    angular.module('angular-toolbox.directives', []);
    angular.module('angular-toolbox.filters', []);




    angular.module('angular-toolbox',
            [
                'angular-toolbox.config',
                'angular-toolbox.directives',
                'angular-toolbox.filters'
            ]);

})(angular);

/**
 * The BfrActions directive creates buttons for form actions (submit...) or a simple button list
 */
'use strict';
angular.module('angular-toolbox').directive('bfrActions', [
    function () {
        return {
            templateUrl: 'templates/bfr-actions.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                options: '=',
                form: '='
            },
            link: function postLink(scope, element, attrs, ngModel) { }
        };
    }
]);

/**
 * Manages a list of checkboxes
 */
'use strict';
angular.module('angular-toolbox').directive('bfrCheckboxList', [
    function () {
        return {
            templateUrl: 'templates/bfr-checkbox-list.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@",
                elementclass: "@",
                req: "@",
                model: "=",
                options: "=",
                value: "@",
                display: "@"
            },
            link: function (scope, element, attrs, ngModel) {
                scope.isValid = true;

                scope.checkValidity = function () {
                    scope.isValid = scope.model.length > 0;
                };

                // Find the index of an object in an array of objects
                function arrayObjectIndexOf(arr, obj) {
                    for (var i = 0; i < arr.length; i++) {
                        if (angular.equals(arr[i], obj)) {
                            return i;
                        }
                    }
                    ;
                    return -1;
                }

                // Init : Check the checkboxes that should be checked
                scope.optionChecked = [];
                var optionIndex = 0;
                angular.forEach(scope.options, function (value, key) {
                    if (angular.isObject(value) && scope.value == null) {   // If array of objects and result should contain objects
                        var i = arrayObjectIndexOf(scope.model, value);     // Try to find the object

                        if (i !== -1) {
                            scope.optionChecked[optionIndex] = true;        // If the item exists in the result, set checked at true
                        } else {
                            scope.optionChecked[optionIndex] = false;       // If the item doesn't exist in the result, set checked at false
                        }

                        optionIndex++;
                    } else if (angular.isObject(value) && scope.value != null) {    // If array of objects and result should contain values
                        var i = scope.model.indexOf(value[scope.value]);

                        if (i !== -1) {
                            scope.optionChecked[optionIndex] = true;
                        } else {
                            scope.optionChecked[optionIndex] = false;
                        }

                        optionIndex++;
                    } else {    // If array of primitive elements
                        var i = scope.model.indexOf(value);

                        if (i !== -1) {
                            scope.optionChecked[optionIndex] = true;
                        } else {
                            scope.optionChecked[optionIndex] = false;
                        }

                        optionIndex++;
                    }
                });

                // Save or remove an element
                scope.toggleElement = function (item) {
                    if (angular.isObject(item) && scope.value == null) {    // If array of objects and result should contain objects
                        var i = arrayObjectIndexOf(scope.model, item);      // Try to find the object

                        if (i !== -1) {
                            scope.model.splice(i, 1);   // If the item exists in the result, remove it
                        } else {
                            scope.model.push(item);     // If the item isn't in the result, add it
                        }

                    } else if (angular.isObject(item) && scope.value != null) {   // If array of objects and result should contain values
                        var i = scope.model.indexOf(item[scope.value]);

                        if (i !== -1) {
                            scope.model.splice(i, 1);
                        } else {
                            scope.model.push(item[scope.value]);
                        }

                    } else {    // If array of values
                        var i = scope.model.indexOf(item);

                        if (i !== -1) {
                            scope.model.splice(i, 1);
                        } else {
                            scope.model.push(item);
                        }
                    }
                };

            }
        };
    }
]);

'use strict';
angular.module('angular-toolbox').directive('bfrCheckbox', [
    function () {
        return {
            templateUrl: 'templates/bfr-checkbox.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@",
                elementclass: "@",
                req: "@",
                model: "=",
                truevalue: "@",
                falsevalue: "@",
                onchange: "&"
            },
            link: function postLink(scope, element, attrs, ngModel) {
            }
        };
    }
]);

/**
 * The Field View directive displays an input and can format it according to its type
 */
'use strict';
angular.module('angular-toolbox').directive('bfrFieldView', [
    function () {
        return {
            //template: '<div></div>',
            templateUrl: 'templates/bfr-field-view.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                field: "=",
                label: "@",
                type: "@"
            }
        };
    }
]);

angular.module("angular-toolbox.directives").run(["$templateCache", function ($templateCache) {
        $templateCache.put("templates/bfr-actions.tpl.html", "<div class=\"bfr-actions\">\n    <button ng-repeat=\"option in options\" class=\"{{option.elementclass}}\" type=\"{{option.type}}\" ng-disabled=\"option.disable ? form.$invalid : false\" style=\"margin: 3px;\" ng-click=\"option.onclick()\">{{option.label}}</button>\n</div>");
        $templateCache.put("templates/bfr-checkbox-list.tpl.html", "<div class=\"form-group {{!isValid ? \'has-error\' : \'\'}}\">\n    <label class=\"control-label\"><i ng-if=\"!isValid\" class=\"material-icons md-14\">warning</i> {{label}} <sup ng-if=\"req\">*</sup></label>\n    <div class=\"checkbox {{elementclass}}\" ng-repeat=\"option in options track by $index\">\n        <label>\n            <input type=\"checkbox\" id=\"{{option}}\" ng-model=\"optionChecked[$index]\" ng-change=\"toggleElement(option)\" ng-required=\"{{req}}\" ng-click=\"checkValidity()\">\n            {{display != null ? option[display] : option}}\n        </label>\n    </div>\n</div>");
        $templateCache.put("templates/bfr-checkbox.tpl.html", "<div class=\"checkbox {{elementclass}}\">\n    <label>\n        <input type=\"checkbox\" ng-model=\"model\" ng-true-value=\"{{truevalue != null ? truevalue : true}}\" ng-false-value=\"{{falsevalue != null ? falsevalue : false}}\" ng-change=\"onchange()\"> {{label}}\n    </label>\n</div>");
        $templateCache.put("templates/bfr-field-view.tpl.html", "<div class=\"form-group\">\n    <label class=\"control-label\">{{label}}</label>\n    <div class=\"form-control\">\n        <span ng-if=\"field != null && field != \'\'\" ng-switch=\"type\">\n            <span ng-switch-when=\"email\"><a ng-href=\"mailto:{{field}}\">{{field}}</a></span>\n            <span ng-switch-when=\"phone\">{{field | phoneFormat}}</span>\n            <span ng-switch-when=\"price\">{{field | currency:\"€\"}}</span>\n            <span ng-switch-when=\"date\">{{field | dateShortFormat}}</span>\n            <span ng-switch-when=\"textarea\">{{field | json}}</span>\n            <span ng-switch-default>{{field}}</span>\n        </span>\n        <span ng-if=\"field == null || field == \'\'\">-</span>\n   </div>\n</div>");
        $templateCache.put("templates/bfr-input-int.tpl.html", "<div class=\"form-group well {{elementclass}}\">\n    <div class=\"row\">\n        <div class=\"col-md-6\">\n            <p>{{label}}</p>\n        </div>\n        <div class=\"col-md-2\">\n            <button type=\"button\" class=\"btn btn-link\" ng-click=\"substract()\">\n                <i class=\"material-icons\">remove</i>\n            </button>\n        </div>\n        <div class=\"col-md-2\">  \n            <p>{{model}}/{{max}}</p>\n        </div>\n        <div class=\"col-md-2\">\n            <button type=\"button\" class=\"btn btn-link\" ng-click=\"add()\">\n                <i class=\"material-icons\">add</i>\n            </button>\n        </div>\n    </div>\n</div>");
        $templateCache.put("templates/bfr-input.tpl.html", "<div class=\"form-group {{!isValid ? \'has-error\' : \'\'}}\">\n    <label class=\"control-label\"><i ng-if=\"!isValid\" class=\"material-icons md-14\">warning</i> {{label}} <sup ng-if=\"req\">*</sup></label>\n    <input type=\"{{type}}\" class=\"form-control {{elementclass}}\" placeholder=\"{{placeholder}}\" step=\"{{step}}\" ng-model=\"model\" ng-required=\"{{req}}\" ng-blur=\"checkValidity()\"/>\n</div>");
        $templateCache.put("templates/bfr-radio.tpl.html", "<div class=\"form-group\">\n    <label class=\"control-label\">{{label}} <sup ng-if=\"req\">*</sup></label>\n    <div class=\"radio {{elementclass}}\" ng-repeat=\"option in options\">\n        <label>\n            <input type=\"radio\" name=\"{{name}}\" ng-model=\"$parent.model\" ng-value=\"option{{value != null ? \'[value]\' : \'\'}}\" ng-required=\"{{req}}\" >\n            {{display != null ? option[display] : option}}\n        </label>\n    </div>\n</div>");
        $templateCache.put("templates/bfr-select.tpl.html", "<div class=\"form-group {{!isValid ? \'has-error\' : \'\'}}\">\n    <label class=\"control-label\"><i ng-if=\"!isValid\" class=\"material-icons md-14\">warning</i> {{label}} <sup ng-if=\"req\">*</sup></label>\n    <select class=\"form-control {{elementclass}}\" ng-options=\"option{{value != null ? \'[value]\' : \'\'}} as option{{display != null ? \'[display]\' : \'\'}} for option in options{{value == null && display != null ? \' track by option.id\' : \'\'}}\" ng-model=\"model\" ng-required=\"{{req}}\" ng-blur=\"checkValidity()\">\n        <option value=\"\" ng-if=\"defaultEnabled\" selected>{{defaultDisplay}}</option>\n    </select>\n</div>");
        $templateCache.put("templates/bfr-selector.tpl.html", "<div class=\"form-group\">\n    <a href=\"\">\n        <span class=\"pull-right\">\n            <i class=\"material-icons\">keyboard_arrow_right</i>\n        </span>\n        {{ label }}\n    </a>\n</div>");
        $templateCache.put("templates/bfr-textarea.tpl.html", "<div class=\"form-group {{!isValid ? \'has-error\' : \'\'}}\">\n    <label class=\"control-label\"><i ng-if=\"!isValid\" class=\"material-icons md-14\">warning</i> {{label}} <sup ng-if=\"req\">*</sup></label>\n    <textarea class=\"form-control {{elementclass}}\" placeholder=\"{{placeholder}}\" ng-model=\"model\" rows=\"{{size}}\" ng-required=\"{{req}}\" ng-blur=\"checkValidity()\"></textarea>\n</div>");
    }]);
'use strict';
angular.module('angular-toolbox').directive('bfrInputInt', [
    function () {
        return {
            templateUrl: 'templates/bfr-input-int.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@",
                elementclass: "@",
                model: "=",
                max: "@",
                min: "@",
                step: "@"
            },
            link: function postLink(scope, element, attrs, ngModel) {
                // Set the model if null at the init
                if (scope.model == null && scope.min != null) {
                    scope.model = scope.min / 1;
                } else if (scope.model == null) {
                    scope.model = 0;
                }

                // Set a step if null at the init
                if (scope.step == null) {
                    scope.step = 1;
                }

                // Add 'step' to the model if the result isn't above max
                scope.add = function () {
                    if (scope.model / 1 + scope.step / 1 <= scope.max) {
                        scope.model = scope.model / 1 + scope.step / 1;
                    }
                };

                // Substract 'step' to the model if the result isn't under min & if there's a min
                scope.substract = function () {
                    if (scope.min != null && scope.model - scope.step >= scope.min || scope.min == null) {
                        scope.model -= scope.step;
                    }
                };

            }
        };
    }
]);

'use strict';
angular.module('angular-toolbox').directive('bfrInput', [
    function () {
        return {
            templateUrl: 'templates/bfr-input.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@",
                elementclass: "@",
                type: "@",
                step: "@",
                placeholder: "@",
                req: "@",
                hasError: "=",
                model: "="
            },
            link: function postLink(scope, element, attrs, ngModel) {
                // Watch the 'hasError' attribute 
                scope.$watch('hasError', function () {
                    scope.isValid = (scope.hasError == null || scope.hasError != true);
                });

                // Check for validity after the element has lost focus
                scope.checkValidity = function () {
                    scope.isValid = element[0].children[1].validity.valid;
                };
            }
        };
    }
]);

'use strict';
angular.module('angular-toolbox').directive('bfrRadio', [
    function () {
        return {
            templateUrl: 'templates/bfr-radio.tpl.html',
            restrict: 'E',
            rtransclude: true,
            replace: true,
            scope: {
                label: "@",
                elementclass: "@",
                req: "@",
                name: "@",
                model: "=",
                options: "=",
                value: "@",
                display: "@"
            }
        };
    }
]);

'use strict';
angular.module('angular-toolbox').directive('bfrSelect', [
    function () {
        return {
            //template: '<div></div>',
            templateUrl: 'templates/bfr-select.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@",
                elementclass: "@",
                req: "@",
                model: "=",
                options: "=",
                value: "@",
                display: "@",
                defaultDisplayEnabled: "@",
                defaultDisplay: "@"
            },
            link: function postLink(scope, element, attrs, ngModel) {
                // Check for validity after the element has lost focus
                // Change display if not valid
                scope.isValid = true;

                scope.defaultEnabled = (scope.defaultDisplayEnabled != null && scope.defaultDisplay != null) ? true : false;

                scope.checkValidity = function () {
                    scope.isValid = element[0].children[1].validity.valid;
                };
            }
        };
    }
]);

'use strict';
angular.module('angular-toolbox').directive('bfrSelector', [
    function () {
        return {
            templateUrl: 'templates/bfr-selector.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@"
            }
        };
    }
]);

'use strict';
angular.module('angular-toolbox').directive('bfrTextarea', [
    function () {
        return {
            templateUrl: 'templates/bfr-textarea.tpl.html',
            restrict: 'E',
            transclude: true,
            replace: true,
            scope: {
                label: "@",
                elementclass: "@",
                size: "@",
                placeholder: "@",
                req: "@",
                model: "="
            },
            link: function postLink(scope, element, attrs, ngModel) {
                // Check for validity after the element has lost focus
                // Change display if not valid
                scope.isValid = true;

                scope.checkValidity = function () {
                    scope.isValid = element[0].children[1].validity.valid;
                };
            }
        };
    }
]);

// Format [France] for a phone number
angular.module('angular-toolbox').filter('phoneFormat', function () {
    return function (input) {
        if (input) {
            var number = input.replace(/\D/g, '');
            var prefix = number.substr(0, 2);

            if (prefix == '08') {
                number = number.replace(/(\d{1})(\d{3})(\d{3})(\d{3})/, '$1 $2 $3 $4');
            } else {
                number = number.replace(/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/, '$1 $2 $3 $4 $5');
            }
            return number;
        } else {
            return input;
        }
    };
});


// Format for a time
angular.module('angular-toolbox').filter('timeFormat', function ($filter) {
    return function (input) {
        if (input) {
            var _date = $filter('date')(new Date(input), 'shortTime');
            return _date;
        } else {
            return input;
        }
    };
});
// Format for a short date
angular.module('angular-toolbox').filter('dateShortFormat', function ($filter) {
    return function (input) {
        if (input) {
            var _date = $filter('date')(new Date(input), 'mediumDate');
            return _date;
        } else {
            return input;
        }
    };
});
// Format for a long date
angular.module('angular-toolbox').filter('dateFullFormat', function ($filter) {
    return function (input) {
        if (input) {
            var _date = $filter('date')(new Date(input), 'longDate');
            return _date;
        } else {
            return input;
        }
    };
});
// Format for a datetime
angular.module('angular-toolbox').filter('datetimeFormat', function ($filter) {
    return function (input) {
        if (input) {
            var _date = $filter('date')(new Date(input), 'short');
            return _date;
        } else {
            return input;
        }
    };
});