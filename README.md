# Angular Toolbox #

An Angular.js set of directives to help the ui development

Dependencies:

* Angular
* Angular Bootstrap
* Angular Gettext (should allow the use of a translation filter like so: {{ 'label' | translate }})
* Bootstrap 3 (css)
* Material icons (css)

### Install with Bower ###

```html
bower install https://bitbucket.org/busineyss/angular-toolbox.git
```

### Usage ###

In your html/template add 

```html
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons" />
<script src="/bower_components/angular-toolbox/dist/angular-toolbox.js"></script>
```

In your application, declare dependency injection like so:

```javascript
angular.module('myApp', ['angular-toolbox']);
```

### Want to customize the toolbox? ###

To create a new directive :

* Put the directive template in /modules/angular-toolbox/directives/templates
* Once completed, copy the template in /dist/templates

Build :

```shell
  cd angular-toolbox
  gulp build
```